﻿using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;

namespace NoStory
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        internal static ManualLogSource Log;
        private ConfigEntry<bool> configEnabled;
        internal static ConfigEntry<bool> ConfigSkipAfterShift;
        internal static ConfigEntry<bool> ConfigSkipMorningDialogue;
        internal static ConfigEntry<bool> ConfigDebugPrint;
        private void Awake()
        {
            Log = base.Logger;
            Log.LogInfo($"Plugin {PluginInfo.PLUGIN_NAME} ({PluginInfo.PLUGIN_GUID}) is loaded!");
            configEnabled = Config.Bind("General", "Enabled", true, 
                "enable / disable whole plugin (only takes effect on restarts)");
            ConfigSkipAfterShift = Config.Bind("General", "SkipAfterShift", true, 
                "skip after shift sequences");
            ConfigSkipMorningDialogue = Config.Bind("General", "SkipMorningDialog", true, 
                "skip selected morning dialogues");
            ConfigDebugPrint = Config.Bind("General", "DebugPrint", false,
                "print (verbose) debug info. meant for development");
            if (configEnabled.Value)
            {
                Log.LogInfo($"patching methods (plugin enabled");
                new Harmony($"{PluginInfo.PLUGIN_GUID}").PatchAll();
                Log.LogInfo($"patched methods");
            }
            else
            {
                Log.LogInfo($"not patching methods (plugin disabled)");
            }
        }
    }
}
