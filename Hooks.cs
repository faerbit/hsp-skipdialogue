﻿using System.Collections.Generic;
using System.Linq;
using BBI.Unity.Game;
using HarmonyLib;

namespace NoStory;

public class Hooks
{
    
    [HarmonyPatch(typeof(PATConditionAsset), nameof(PATConditionAsset.AreConditionsMet))]
    public class PATConditionAsset_AreConditionsMet
    {
        private static readonly IEnumerable<string> BlockedPATCAs = new HashSet<string>
        {
            "PATCon_07_00_CalyssiaAntiUnion",
            "PATCon_09_00_RhodesArrives",
            "PATCon_17_X2_LynxUnionClampdown",
            "XX_01 - Data Drive - PAT Con_PATConditionAsset",
        };
        public static void Postfix(ref PATConditionAsset __instance, ref bool __result)
        {
            if (Plugin.ConfigDebugPrint.Value)
            {
                Plugin.Log.LogInfo($"PATConditionAsset.AreConditionsMet: \"{__instance.name}\" -> {__result}");
            }
            
            if (__result && BlockedPATCAs.Contains(__instance.name))
            {
                if (Plugin.ConfigSkipMorningDialogue.Value)
                {
                    Plugin.Log.LogInfo($"PATConditionAsset.AreConditionsMet: blocking {__instance.name}");
                    __result = false;
                }
                else
                {
                    Plugin.Log.LogInfo($"PATConditionAsset.AreConditionsMet: not blocking {__instance.name}");
                }
            }
        }
    }
    
    [HarmonyPatch(typeof(Hab3DController), nameof(Hab3DController.SetAfterShiftHabMode))]
    public class Hab3DController_SetAfterShiftHabMode
    {
        public static bool Prefix(HabAfterShiftAsset afterShiftSettings)
        {
            Plugin.Log.LogInfo($"Hab3DController.SetAfterShiftHabMode: {afterShiftSettings}");
            if (Plugin.ConfigSkipAfterShift.Value)
            {
                
                Plugin.Log.LogInfo($"Hab3DController.SetAfterShiftHabMode: blocking");
                return false;
            }
            else
            {
                Plugin.Log.LogInfo($"Hab3DController.SetAfterShiftHabMode: not blocking");
                return true;
            }
        }
    }
}